#!/bin/bash

# Copy all PHP files in the directory to RSS Bridge
for extension in ./*.php; do
	echo "Copying $extension..."
	docker cp ./"$extension" rss-bridge:/app/bridges
done

# White-list all bridges
docker exec -it rss-bridge bash -c "echo '*' > /app/whitelist.txt"

# Enable DEBUG Mode
docker exec -it rss-bridge bash -c "touch /app/DEBUG"
