# My Custom bridges for RSS-Bridge

If you don't know what RSS-Bridge is, visit its GitHub page: https://github.com/RSS-Bridge/rss-bridge

This repo contains modified versions of some bridges from RSS-Bridge, to better fit my needs.

It also contains a script to automatically load these bridges into RSS-Bridge **docker instance**

Feel free to take a look at the code, to modify it and do whatever you want to it.

Don't trust me? That's fine! You can verify what I've changed in the original bridges by downloading them from the official RSS-Bridge and running 'git diff' to compare the original version and my custom version.

Example:
```
git diff FilterBridge.php FilterDWBridge.php
```

I always add "DW" before the "Bridge", in the file's name.

## Installing RSS-Bridge (copied from https://github.com/RSS-Bridge/rss-bridge#install-with-docker)

Install with Docker:

Install by using docker image from Docker Hub:

```
# Create container
docker create --name=rss-bridge --publish 3000:80 rssbridge/rss-bridge

# Start container
docker start rss-bridge
```

Browse http://localhost:3000/

## License
MIT, as always...
