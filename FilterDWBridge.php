<?php

class FilterDWBridge extends FeedExpander
{
    const MAINTAINER = 'Frenzie, ORelio';
    const NAME = 'FilterDW';
    const CACHE_TIMEOUT = 3600; // 1h
    const DESCRIPTION = 'Filters a feed of your choice';
    const URI = 'https://github.com/RSS-Bridge/rss-bridge';

    const PARAMETERS = [[
        'url' => [
            'name' => 'Feed URL',
            'type'  => 'text',
            'defaultValue' => 'https://lorem-rss.herokuapp.com/feed?unit=day',
            'required' => true,
        ],
        'filter' => [
            'name' => 'Filter (regular expression!!!)',
            'required' => false,
        ],
        'filter_type' => [
            'name' => 'Filter type',
            'type' => 'list',
            'required' => false,
            'values' => [
                'Keep matching items' => 'permit',
                'Hide matching items' => 'block',
            ],
            'defaultValue' => 'permit',
        ],
        'case_insensitive' => [
            'name' => 'Case-insensitive filter',
            'type' => 'checkbox',
            'required' => false,
        ],
        'fix_encoding' => [
            'name' => 'Attempt Latin1/UTF-8 fixes when evaluating filter',
            'type' => 'checkbox',
            'required' => false,
        ],
        'target_author' => [
            'name' => 'Apply filter on author',
            'type' => 'checkbox',
            'required' => false,
        ],
        'target_content' => [
            'name' => 'Apply filter on content',
            'type' => 'checkbox',
            'required' => false,
        ],
        'target_title' => [
            'name' => 'Apply filter on title',
            'type' => 'checkbox',
            'required' => false,
            'defaultValue' => 'checked'
        ],
        'target_uri' => [
            'name' => 'Apply filter on URI/URL',
            'type' => 'checkbox',
            'required' => false,
        ],
        'title_from_content' => [
            'name' => 'Generate title from content (overwrite existing title)',
            'type' => 'checkbox',
            'required' => false,
        ],
        'today_items' => [
            'name' => 'Only items from today',
            'type' => 'checkbox',
            'required' => false,
        ],
        'relative_timezone' => [
            'name' => 'Relative Timezone',
            'type' => 'list',
            'required' => false,
            'values' => [
			    '(GMT-11:00) Midway Island' => 'Pacific/Midway',
			    '(GMT-11:00) Samoa' => 'Pacific/Samoa',
			    '(GMT-10:00) Hawaii' => 'Pacific/Honolulu',
			    '(GMT-09:00) Alaska' => 'US/Alaska',
			    '(GMT-08:00) Pacific Time (US &amp; Canada)' => 'America/Los_Angeles',
			    '(GMT-08:00) Tijuana' => 'America/Tijuana',
			    '(GMT-07:00) Arizona' => 'US/Arizona',
			    '(GMT-07:00) Chihuahua' => 'America/Chihuahua',
			    '(GMT-07:00) La Paz' => 'America/Chihuahua',
			    '(GMT-07:00) Mazatlan' => 'America/Mazatlan',
			    '(GMT-07:00) Mountain Time (US &amp; Canada)' => 'US/Mountain',
			    '(GMT-06:00) Central America' => 'America/Managua',
			    '(GMT-06:00) Central Time (US &amp; Canada)' => 'US/Central',
			    '(GMT-06:00) Guadalajara' => 'America/Mexico_City',
			    '(GMT-06:00) Mexico City' => 'America/Mexico_City',
			    '(GMT-06:00) Monterrey' => 'America/Monterrey',
			    '(GMT-06:00) Saskatchewan' => 'Canada/Saskatchewan',
			    '(GMT-05:00) Bogota' => 'America/Bogota',
			    '(GMT-05:00) Eastern Time (US &amp; Canada)' => 'US/Eastern',
			    '(GMT-05:00) Indiana (East)' => 'US/East-Indiana',
			    '(GMT-05:00) Lima' => 'America/Lima',
			    '(GMT-05:00) Quito' => 'America/Bogota',
			    '(GMT-04:00) Atlantic Time (Canada)' => 'Canada/Atlantic',
			    '(GMT-04:30) Caracas' => 'America/Caracas',
			    '(GMT-04:00) La Paz' => 'America/La_Paz',
			    '(GMT-04:00) Santiago' => 'America/Santiago',
			    '(GMT-03:30) Newfoundland' => 'Canada/Newfoundland',
			    '(GMT-03:00) Brasilia' => 'America/Sao_Paulo',
			    '(GMT-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires',
			    '(GMT-03:00) Georgetown' => 'America/Argentina/Buenos_Aires',
			    '(GMT-03:00) Greenland' => 'America/Godthab',
			    '(GMT-02:00) Mid-Atlantic' => 'America/Noronha',
			    '(GMT-01:00) Azores' => 'Atlantic/Azores',
			    '(GMT-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde',
			    '(GMT+00:00) Casablanca' => 'Africa/Casablanca',
			    '(GMT+00:00) Edinburgh' => 'Europe/London',
			    '(GMT+00:00) Greenwich Mean Time : Dublin' => 'Etc/Greenwich',
			    '(GMT+00:00) Lisbon' => 'Europe/Lisbon',
			    '(GMT+00:00) London' => 'Europe/London',
			    '(GMT+00:00) Monrovia' => 'Africa/Monrovia',
			    '(GMT+00:00) UTC' => 'UTC',
			    '(GMT+01:00) Amsterdam' => 'Europe/Amsterdam',
			    '(GMT+01:00) Belgrade' => 'Europe/Belgrade',
			    '(GMT+01:00) Berlin' => 'Europe/Berlin',
			    '(GMT+01:00) Bern' => 'Europe/Berlin',
			    '(GMT+01:00) Bratislava' => 'Europe/Bratislava',
			    '(GMT+01:00) Brussels' => 'Europe/Brussels',
			    '(GMT+01:00) Budapest' => 'Europe/Budapest',
			    '(GMT+01:00) Copenhagen' => 'Europe/Copenhagen',
			    '(GMT+01:00) Ljubljana' => 'Europe/Ljubljana',
			    '(GMT+01:00) Madrid' => 'Europe/Madrid',
			    '(GMT+01:00) Paris' => 'Europe/Paris',
			    '(GMT+01:00) Prague' => 'Europe/Prague',
			    '(GMT+01:00) Rome' => 'Europe/Rome',
			    '(GMT+01:00) Sarajevo' => 'Europe/Sarajevo',
			    '(GMT+01:00) Skopje' => 'Europe/Skopje',
			    '(GMT+01:00) Stockholm' => 'Europe/Stockholm',
			    '(GMT+01:00) Vienna' => 'Europe/Vienna',
			    '(GMT+01:00) Warsaw' => 'Europe/Warsaw',
			    '(GMT+01:00) West Central Africa' => 'Africa/Lagos',
			    '(GMT+01:00) Zagreb' => 'Europe/Zagreb',
			    '(GMT+02:00) Athens' => 'Europe/Athens',
			    '(GMT+02:00) Bucharest' => 'Europe/Bucharest',
			    '(GMT+02:00) Cairo' => 'Africa/Cairo',
			    '(GMT+02:00) Harare' => 'Africa/Harare',
			    '(GMT+02:00) Helsinki' => 'Europe/Helsinki',
			    '(GMT+02:00) Istanbul' => 'Europe/Istanbul',
			    '(GMT+02:00) Jerusalem' => 'Asia/Jerusalem',
			    '(GMT+02:00) Kyiv' => 'Europe/Helsinki',
			    '(GMT+02:00) Pretoria' => 'Africa/Johannesburg',
			    '(GMT+02:00) Riga' => 'Europe/Riga',
			    '(GMT+02:00) Sofia' => 'Europe/Sofia',
			    '(GMT+02:00) Tallinn' => 'Europe/Tallinn',
			    '(GMT+02:00) Vilnius' => 'Europe/Vilnius',
			    '(GMT+03:00) Baghdad' => 'Asia/Baghdad',
			    '(GMT+03:00) Kuwait' => 'Asia/Kuwait',
			    '(GMT+03:00) Minsk' => 'Europe/Minsk',
			    '(GMT+03:00) Nairobi' => 'Africa/Nairobi',
			    '(GMT+03:00) Riyadh' => 'Asia/Riyadh',
			    '(GMT+03:00) Volgograd' => 'Europe/Volgograd',
			    '(GMT+03:30) Tehran' => 'Asia/Tehran',
			    '(GMT+04:00) Abu Dhabi' => 'Asia/Muscat',
			    '(GMT+04:00) Baku' => 'Asia/Baku',
			    '(GMT+04:00) Moscow' => 'Europe/Moscow',
			    '(GMT+04:00) Muscat' => 'Asia/Muscat',
			    '(GMT+04:00) St. Petersburg' => 'Europe/Moscow',
			    '(GMT+04:00) Tbilisi' => 'Asia/Tbilisi',
			    '(GMT+04:00) Yerevan' => 'Asia/Yerevan',
			    '(GMT+04:30) Kabul' => 'Asia/Kabul',
			    '(GMT+05:00) Islamabad' => 'Asia/Karachi',
			    '(GMT+05:00) Karachi' => 'Asia/Karachi',
			    '(GMT+05:00) Tashkent' => 'Asia/Tashkent',
			    '(GMT+05:30) Chennai' => 'Asia/Calcutta',
			    '(GMT+05:30) Kolkata' => 'Asia/Kolkata',
			    '(GMT+05:30) Mumbai' => 'Asia/Calcutta',
			    '(GMT+05:30) New Delhi' => 'Asia/Calcutta',
			    '(GMT+05:30) Sri Jayawardenepura' => 'Asia/Calcutta',
			    '(GMT+05:45) Kathmandu' => 'Asia/Katmandu',
			    '(GMT+06:00) Almaty' => 'Asia/Almaty',
			    '(GMT+06:00) Astana' => 'Asia/Dhaka',
			    '(GMT+06:00) Dhaka' => 'Asia/Dhaka',
			    '(GMT+06:00) Ekaterinburg' => 'Asia/Yekaterinburg',
			    '(GMT+06:30) Rangoon' => 'Asia/Rangoon',
			    '(GMT+07:00) Bangkok' => 'Asia/Bangkok',
			    '(GMT+07:00) Hanoi' => 'Asia/Bangkok',
			    '(GMT+07:00) Jakarta' => 'Asia/Jakarta',
			    '(GMT+07:00) Novosibirsk' => 'Asia/Novosibirsk',
			    '(GMT+08:00) Beijing' => 'Asia/Hong_Kong',
			    '(GMT+08:00) Chongqing' => 'Asia/Chongqing',
			    '(GMT+08:00) Hong Kong' => 'Asia/Hong_Kong',
			    '(GMT+08:00) Krasnoyarsk' => 'Asia/Krasnoyarsk',
			    '(GMT+08:00) Kuala Lumpur' => 'Asia/Kuala_Lumpur',
			    '(GMT+08:00) Perth' => 'Australia/Perth',
			    '(GMT+08:00) Singapore' => 'Asia/Singapore',
			    '(GMT+08:00) Taipei' => 'Asia/Taipei',
			    '(GMT+08:00) Ulaan Bataar' => 'Asia/Ulan_Bator',
			    '(GMT+08:00) Urumqi' => 'Asia/Urumqi',
			    '(GMT+09:00) Irkutsk' => 'Asia/Irkutsk',
			    '(GMT+09:00) Osaka' => 'Asia/Tokyo',
			    '(GMT+09:00) Sapporo' => 'Asia/Tokyo',
			    '(GMT+09:00) Seoul' => 'Asia/Seoul',
			    '(GMT+09:00) Tokyo' => 'Asia/Tokyo',
			    '(GMT+09:30) Adelaide' => 'Australia/Adelaide',
			    '(GMT+09:30) Darwin' => 'Australia/Darwin',
			    '(GMT+10:00) Brisbane' => 'Australia/Brisbane',
			    '(GMT+10:00) Canberra' => 'Australia/Canberra',
			    '(GMT+10:00) Guam' => 'Pacific/Guam',
			    '(GMT+10:00) Hobart' => 'Australia/Hobart',
			    '(GMT+10:00) Melbourne' => 'Australia/Melbourne',
			    '(GMT+10:00) Port Moresby' => 'Pacific/Port_Moresby',
			    '(GMT+10:00) Sydney' => 'Australia/Sydney',
			    '(GMT+10:00) Yakutsk' => 'Asia/Yakutsk',
			    '(GMT+11:00) Vladivostok' => 'Asia/Vladivostok',
			    '(GMT+12:00) Auckland' => 'Pacific/Auckland',
			    '(GMT+12:00) Fiji' => 'Pacific/Fiji',
			    '(GMT+12:00) International Date Line West' => 'Pacific/Kwajalein',
			    '(GMT+12:00) Kamchatka' => 'Asia/Kamchatka',
			    '(GMT+12:00) Magadan' => 'Asia/Magadan',
			    '(GMT+12:00) Marshall Is.' => 'Pacific/Fiji',
			    '(GMT+12:00) New Caledonia' => 'Asia/Magadan',
			    '(GMT+12:00) Solomon Is.' => 'Asia/Magadan',
			    '(GMT+12:00) Wellington' => 'Pacific/Auckland',
			    '(GMT+13:00) Nuku\'alofa' => 'Pacific/Tongatapu',
            ],
            'defaultValue' => 'America/Sao_Paulo',
        ],

        'length_limit' => [
            'name' => 'Max length analyzed by filter (-1: no limit)',
            'type' => 'number',
            'required' => false,
            'defaultValue' => -1,
        ],
    ]];

	// https://stackoverflow.com/a/6369355
	CONST TIMEZONES = [[
    	]];

    private $request = '';

    protected function parseItem($newItem)
    {
		$item = parent::parseItem($newItem);

		// Check if item is from today. If not, skip item.
		$today = (new DateTime("today", new DateTimeZone($this->getInput('relative_timezone')) ))->getTimestamp();
		$tomorrow = (new DateTime("tomorrow", new DateTimeZone($this->getInput('relative_timezone')) ))->getTimestamp();
		$pubDate = (new DateTime(date('m/d/Y H:i:s', $item['timestamp']), new DateTimeZone($this->getInput('relative_timezone')) ))->getTimestamp();

		if ($this->getInput('today_items') && ($pubDate < $today || $pubDate >= $tomorrow)) {
		    return null;
		}

        // Generate title from first 50 characters of content?
        if ($this->getInput('title_from_content') && array_key_exists('content', $item)) {
            $content = str_get_html($item['content']);
            $pos = strpos($item['content'], ' ', 50);
            $item['title'] = substr($content->plaintext, 0, $pos);
            if (strlen($content->plaintext) >= $pos) {
                $item['title'] .= '...';
            }
        }

        $filter = $this->getInput('filter');
        if (! str_contains($filter, '#')) {
            $delimiter = '#';
        } elseif (! str_contains($filter, '/')) {
            $delimiter = '/';
        } else {
            throw new \Exception('Cannot use both / and # inside filter');
        }

        $regex = $delimiter . $filter . $delimiter;
        if ($this->getInput('case_insensitive')) {
            $regex .= 'i';
        }

        // Retrieve fields to check
        $filter_fields = [];
        if ($this->getInput('target_author')) {
            $filter_fields[] = $item['author'] ?? null;
        }
        if ($this->getInput('target_content')) {
            $filter_fields[] = $item['content'] ?? null;
        }
        if ($this->getInput('target_title')) {
            $filter_fields[] = $item['title'] ?? null;
        }
        if ($this->getInput('target_uri')) {
            // todo: maybe consider 'http' and 'https' equivalent? Also maybe optionally .www subdomain?
            $filter_fields[] = $item['uri'] ?? null;
        }

        // Apply filter on item
        $keep_item = false;
        $length_limit = intval($this->getInput('length_limit'));
        foreach ($filter_fields as $field) {
            if ($length_limit > 0) {
                $field = substr($field, 0, $length_limit);
            }
            $result = preg_match($regex, $field);
            if ($result === false) {
                // todo: maybe notify user about the error here?
            }
            $keep_item |= boolval($result);
            if ($this->getInput('fix_encoding')) {
                $keep_item |= boolval(preg_match($regex, utf8_decode($field)));
                $keep_item |= boolval(preg_match($regex, utf8_encode($field)));
            }
        }

        // Reverse result? (keep everything but matching items)
        if ($this->getInput('filter_type') === 'block') {
            $keep_item = !$keep_item;
        }

        return $keep_item ? $item : null;
    }

    public function getURI()
    {
        $url = $this->getInput('url');

        if (empty($url)) {
            $url = parent::getURI();
        }

        return $url;
    }

    public function collectData()
    {
        if ($this->getInput('url') && substr($this->getInput('url'), 0, 4) !== 'http') {
            // just in case someone finds a way to access local files by playing with the url
            returnClientError('The url parameter must either refer to http or https protocol.');
        }
        $this->collectExpandableDatas($this->getURI());
    }
}
